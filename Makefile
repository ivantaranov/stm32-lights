
TARGET = stm32-lights
DEVICE = stm32f103

SOURCES = \
dsp/cr4_fft_256_stm32.s \
sqrr.c \
startup.c \
chip.c \
main.c \

AS=/media/storage/arm-none-eabi-gcc-m3/bin/arm-none-eabi-as
CC=/media/storage/arm-none-eabi-gcc-m3/bin/arm-none-eabi-gcc
LD=/media/storage/arm-none-eabi-gcc-m3/bin/arm-none-eabi-gcc
CP=/media/storage/arm-none-eabi-gcc-m3/bin/arm-none-eabi-objcopy
DD=/media/storage/arm-none-eabi-gcc-m3/bin/arm-none-eabi-objdump
SZ=/media/storage/arm-none-eabi-gcc-m3/bin/arm-none-eabi-size
STL=/usr/bin/st-flash --reset

SRCDIR = src
BUILDDIR = build

INC = -I$(SRCDIR) \
-I/home/alex/Documents/projects/libs/stm32/STM32CubeF1/Drivers/CMSIS/Device/ST/STM32F1xx/Include \
-I/home/alex/Documents/projects/libs/stm32/STM32CubeF1/Drivers/CMSIS/Core/Include \

CFLAGS=-Wall -std=gnu11 -Os -g -mcpu=cortex-m3 -mthumb
LDFLAGS=-nostartfiles -fstack-usage

OBJECTS=$(filter %.o, \
$(patsubst %.s, $(BUILDDIR)/%.o, $(SOURCES)) \
$(patsubst %.c, $(BUILDDIR)/%.o, $(SOURCES)))

all: $(TARGET)

$(TARGET): $(OBJECTS) 
	$(CC) $(CFLAGS) $(LDFLAGS) -T$(DEVICE).ld -Wl,-Map=$(BUILDDIR)/$(TARGET).map -o $(BUILDDIR)/$(TARGET).elf $(OBJECTS) -lm
	$(DD) -marm -Mforce-thumb -S $(BUILDDIR)/$(TARGET).elf >$(BUILDDIR)/$(TARGET).lst
	$(CP) -O binary $(BUILDDIR)/$(TARGET).elf $(BUILDDIR)/$(TARGET).bin
	$(SZ) $(BUILDDIR)/$(TARGET).elf

$(BUILDDIR)/%.o: $(SRCDIR)/%.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

$(BUILDDIR)/%.o: $(SRCDIR)/%.s
	@mkdir -p $(@D)
	$(AS) -o $@ $<

clean:
	@rm -fr $(BUILDDIR)

install:
	$(STL) erase
	$(STL) write $(BUILDDIR)/$(TARGET).bin 0x08000000
