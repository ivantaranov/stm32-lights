/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   chip.h
 * Author: alex
 *
 * Created on 5 декабря 2020 г., 9:30
 */

#ifndef CHIP_H
#define CHIP_H

#include "stm32f103x6.h"

#define PWM_MAX 255

typedef enum {
    TMR_LEDR,
    TMR_LEDG,
    TMR_LEDB,
    TMR_CMU,
    TMR_MAX
}
TIMERS;

void chip_init(void);

uint64_t get_ms(void);

void dbg_write(char *str);
void dbg_dec(int v);
void dbg_hex(uint32_t digit, uint8_t len);
void dbg_dump(uint8_t *buf, int len);
void usart_rx_byte(uint8_t b);

void spi_io(uint8_t *buf, uint8_t len);
unsigned spi_busy(void);

void set_timer(uint8_t num, uint32_t delay);
unsigned get_timer(uint8_t num);

#define TMR_IDLE 0
#define TMR_ONCE 1
#define TMR_RUN 2
#define TMR_STOP UINT32_MAX

typedef struct {
    uint32_t r0;
    uint32_t r1;
    uint32_t r2;
    uint32_t r3;
    uint32_t r12;
    uint32_t lr;
    uint32_t pc;
    uint32_t psr;
}
FAULT_TRACE;

#define BIT0 (uint8_t)(1 << 0)
#define BIT1 (uint8_t)(1 << 1)
#define BIT2 (uint8_t)(1 << 2)
#define BIT3 (uint8_t)(1 << 3)
#define BIT4 (uint8_t)(1 << 4)
#define BIT5 (uint8_t)(1 << 5)
#define BIT6 (uint8_t)(1 << 6)
#define BIT7 (uint8_t)(1 << 7)

#define BIT8 (uint16_t)(1 << 8)
#define BIT9 (uint16_t)(1 << 9)
#define BIT10 (uint16_t)(1 << 10)
#define BIT11 (uint16_t)(1 << 11)
#define BIT12 (uint16_t)(1 << 12)
#define BIT13 (uint16_t)(1 << 13)
#define BIT14 (uint16_t)(1 << 14)
#define BIT15 (uint16_t)(1 << 15)

#define BIT16 (uint32_t)(1 << 16)
#define BIT17 (uint32_t)(1 << 17)
#define BIT18 (uint32_t)(1 << 18)
#define BIT19 (uint32_t)(1 << 19)
#define BIT20 (uint32_t)(1 << 20)
#define BIT21 (uint32_t)(1 << 21)
#define BIT22 (uint32_t)(1 << 22)
#define BIT23 (uint32_t)(1 << 23)

#define BIT24 (uint32_t)(1 << 24)
#define BIT25 (uint32_t)(1 << 25)
#define BIT26 (uint32_t)(1 << 26)
#define BIT27 (uint32_t)(1 << 27)
#define BIT28 (uint32_t)(1 << 28)
#define BIT29 (uint32_t)(1 << 29)
#define BIT30 (uint32_t)(1 << 30)
#define BIT31 (uint32_t)(1 << 31)

#endif /* CHIP_H */

