/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdint.h>

extern unsigned int _DATA_ROM_START;
extern unsigned int _DATA_RAM_START;
extern unsigned int _DATA_RAM_END;
extern unsigned int _BSS_START;
extern unsigned int _BSS_END;

#define ISR __attribute__   ((interrupt))
#define VECT __attribute__  ((section(".vectors")))
#define STACK_TOP           0x20000400

void startup();
int main(void);

void HARDFAULT_IRQ(void) ISR;
void SYSTICK_IRQ(void) ISR;
void USART1_IRQ(void) ISR;
void SPI1_IRQ(void) ISR;
void DMA1_Channel1_IRQ(void) ISR;

const void * vect[76] VECT = {
    (void *) STACK_TOP, startup, 0, HARDFAULT_IRQ,
    0, 0, 0, 0, // 0x010-0x1f
    0, 0, 0, 0, // 0x020-0x0x02f
    0, 0, 0, SYSTICK_IRQ, // 0x030-0x03f
    0, 0, 0, 0, // 0x040-0x04f
    0, 0, 0, 0, // 0x050-0x05f
    0, 0, 0, DMA1_Channel1_IRQ, // 0x060-0x06f
    0, 0, 0, 0, // 0x070-0x07f
    0, 0, 0, 0, // 0x080-0x08f
    0, 0, 0, 0, // 0x090-0x09f
    0, 0, 0, 0, // 0x0a0-0x0af
    0, 0, 0, 0, // 0x0b0-0x0bf
    0, 0, 0, SPI1_IRQ, // 0x0c0-0x0cf
    0, USART1_IRQ, 0, 0, // 0x0d0-0x0df
    0, 0, 0, 0, // 0x0e0-0x0ef
    0, 0, 0, 0, // 0x0f0-0x0ff
    0, 0, 0, 0, // 0x100-0x10f
    0, 0, 0, 0, // 0x110-0x11f
    0, 0, 0, 0 // 0x120-0x12f
};

void startup(void) {

    for (uint32_t i = 0; i < 500000; i++) __asm__ volatile ("nop");

    unsigned int * data_rom_start_p = &_DATA_ROM_START;
    unsigned int * data_ram_start_p = &_DATA_RAM_START;
    unsigned int * data_ram_end_p = &_DATA_RAM_END;

    while (data_ram_start_p != data_ram_end_p) {
        *data_ram_start_p = *data_rom_start_p;
        data_ram_start_p++;
        data_rom_start_p++;
    }

    unsigned int * bss_start_p = &_BSS_START;
    unsigned int * bss_end_p = &_BSS_END;

    while (bss_start_p != bss_end_p) {
        *bss_start_p = 0;
        bss_start_p++;
    }

    main();
}
