
#define HSE_TO 500000

#include "chip.h"
#include <stdlib.h>
#include <string.h>

static uint32_t tmr[TMR_MAX];
static uint64_t systimems = 0;

uint8_t *spi_buf = 0;
uint8_t spi_len = 0;

#define DBG_BUF_SZ 1000

static uint8_t dbg_buf[DBG_BUF_SZ] __attribute__ ((section(".persistent")));
static uint16_t dbg_buf_in = 0;
static uint16_t dbg_buf_out = 0;
static uint16_t dbg_buf_len = 0;

static FAULT_TRACE fault_trace __attribute__ ((section(".persistent")));
static uint32_t scb_hfsr __attribute__ ((section(".persistent")));
static uint32_t scb_cfsr __attribute__ ((section(".persistent")));
static uint32_t scb_cfsr __attribute__ ((section(".persistent")));
static uint32_t scb_bfar __attribute__ ((section(".persistent")));
static uint32_t fault_trace_flag __attribute__ ((section(".persistent")));

#define FAULT_TRACE_FLAG 0xdeadbeef

typedef struct {
    uint32_t mask;
    char *str;
}
DBG_CFSR_STR;

void dbg_cfsr(uint32_t cfsr_reg) {
    const DBG_CFSR_STR dbg_cfsr_str[] = {
        {BIT25, "DIVBYZERO"},
        {BIT24, "UNALIGNED"},
        {BIT19, "NOCP"},
        {BIT18, "INVPC"},
        {BIT17, "INVSTATE"},
        {BIT16, "UNDEFINSTR"},
        {BIT15, "BFARVALID"},
        {BIT12, "STKERR"},
        {BIT11, "UNSTKERR"},
        {BIT10, "IMPRECISERR"},
        {BIT9, "PRECISERR"},
        {BIT8, "IBUSERR"},
        {BIT7, "MMARVALID"},
        {BIT4, "MSTKERR"},
        {BIT3, "MUNSTKERR"},
        {BIT1, "DACCVIOL"},
        {BIT0, "IACCVIOL"},
        {0, ""},
    };
    for (int i = 0; dbg_cfsr_str[i].mask; i++) {
        if ((cfsr_reg & dbg_cfsr_str[i].mask) == 0) continue;
        dbg_write("    ");
        dbg_write(dbg_cfsr_str[i].str);
        dbg_write("\r\n");
    }
}

void chip_init(void) {

    RCC->CR |= RCC_CR_HSEON;
    while ((RCC->CR & RCC_CR_HSERDY) == 0);

    RCC->CFGR |= RCC_CFGR_PLLMULL9 // 72MHz
            | RCC_CFGR_PLLSRC
            | RCC_CFGR_ADCPRE_DIV8
            | RCC_CFGR_PPRE1_DIV2;

    FLASH->ACR |= FLASH_ACR_LATENCY_2;

    RCC->CR |= RCC_CR_PLLON;
    while ((RCC->CR & RCC_CR_PLLRDY) == 0);

    RCC->CFGR |= RCC_CFGR_SW_PLL;

    RCC->CR &= ~RCC_CR_HSION;
    while (RCC->CR & RCC_CR_HSIRDY);

    for (int i = 0; i < TMR_MAX; i++) {
        tmr[i] = TMR_STOP;
    }

    SysTick->LOAD = 72000;
    SysTick->CTRL = 7;

    /* GPIO */

    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN
            | RCC_APB2ENR_IOPBEN
            | RCC_APB2ENR_IOPCEN;

    /* LED PC13 */

    GPIOC->CRH &= 0xff0fffff;
    GPIOC->CRH |= 0x00200000;

    /* USART1 */

    GPIOA->CRH &= 0xfffff000;
    GPIOA->CRH |= 0x000004a2; // A8 - Gnd

    RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

    USART1->BRR = 0x0271; // 115200
    USART1->CR1 = USART_CR1_UE | USART_CR1_RXNEIE | USART_CR1_TE | USART_CR1_RE;

    __NVIC_EnableIRQ(USART1_IRQn);

    __asm volatile ("cpsie i" : : : "memory");

    if (fault_trace_flag == FAULT_TRACE_FLAG) {
        fault_trace_flag = 0;
        dbg_write("\f*** HardFault ***\r\n");
        dbg_write("\r\nr0: ");
        dbg_hex(fault_trace.r0, 8);
        dbg_write("\r\nr1: ");
        dbg_hex(fault_trace.r1, 8);
        dbg_write("\r\nr2: ");
        dbg_hex(fault_trace.r2, 8);
        dbg_write("\r\nr3: ");
        dbg_hex(fault_trace.r3, 8);
        dbg_write("\r\nr12: ");
        dbg_hex(fault_trace.r12, 8);
        dbg_write("\r\nlr: ");
        dbg_hex(fault_trace.lr, 8);
        dbg_write("\r\npc: ");
        dbg_hex(fault_trace.pc, 8);
        dbg_write("\r\npsr: ");
        dbg_hex(fault_trace.psr, 8);
        dbg_write("\r\nhfsr: ");
        dbg_hex(scb_hfsr, 8);
        dbg_write("\r\ncfsr:\r\n");
        dbg_cfsr(scb_cfsr);
        dbg_write("bfar: ");
        dbg_hex(scb_bfar, 8);
        for (;;);
    }
}

void chip_reset(void) {
    SCB->AIRCR = 0x05FA0004;
}

void set_timer(uint8_t num, uint32_t delay) {
    if (num >= TMR_MAX) return;
    tmr[num] = delay;
}

unsigned get_timer(uint8_t num) {
    if (num >= TMR_MAX) return TMR_IDLE;
    if (tmr[num] == TMR_STOP) return TMR_IDLE;
    if (tmr[num] > 0) return TMR_RUN;
    tmr[num] = TMR_STOP;
    return TMR_ONCE;
}

void SYSTICK_IRQ(void) {
    for (int i = 0; i < TMR_MAX; i++) {
        if ((tmr[i] > 0) && (tmr[i] < TMR_STOP)) tmr[i]--;
    }
    systimems++;
}

uint64_t get_ms(void) {
    return systimems;
}

char hex(uint8_t b) {
    b &= 0xf;
    if (b < 10) return '0' + b;
    return ('A' - 10) +b;
}

void dbg_dec(int v) {
    char buf[20];
    itoa(v, buf, 10);
    dbg_write(buf);
}

void dbg_hex(uint32_t digit, uint8_t len) {
    char a[20];
    uint8_t i = 19;
    a[19] = 0;
    for (;;) {
        char d = digit % 16;
        if (d < 10) d += 0x30;
        else d += 55;
        a[--i] = d;
        digit >>= 4;
        if (i == sizeof a - 1 - len) break;
    }
    dbg_write(&a[i]);
}

void dbg_dump(uint8_t *buf, int len) {
    int i;
    for (i = 0; i < len; i++) {
        if ((i & 0x0f) > 0) dbg_write(" ");
        dbg_hex(buf[i], 2);
        if ((i & 0x0f) == 0x0f) dbg_write("\r\n");
    }
    if ((i & 0x0f) < 0x0f) dbg_write("\r\n");
}

void dbg_write(char *str) {
    while (*str) {
        while (dbg_buf_len >= (DBG_BUF_SZ - 1)) __asm__ volatile ("nop");
        USART1->CR1 &= ~USART_CR1_TXEIE;
        __asm__ volatile ("nop");
        dbg_buf[dbg_buf_in++] = *(str++);
        if (dbg_buf_in >= DBG_BUF_SZ) dbg_buf_in = 0;
        dbg_buf_len++;
        USART1->CR1 |= USART_CR1_TXEIE;
    }
}

void USART1_IRQ(void) {
    if (USART1->SR & USART_SR_TXE) {
        if (dbg_buf_len == 0) {
            USART1->CR1 &= ~USART_CR1_TXEIE;
        } else {
            USART1->DR = dbg_buf[dbg_buf_out++];
            if (dbg_buf_out >= DBG_BUF_SZ) dbg_buf_out = 0;
            dbg_buf_len--;
        }
    }
    if (USART1->SR & USART_SR_RXNE) {
        usart_rx_byte(USART1->DR);
    }
}

void spi_io(uint8_t *buf, uint8_t len) {
    spi_buf = buf;
    spi_len = len;
    SPI1->DR = *buf;
    SPI1->CR2 |= BIT6;
}

unsigned spi_busy(void) {
    return (SPI1->SR & BIT7);
}

void SPI1_IRQ(void) {
    *spi_buf = SPI1->DR;
    spi_buf++;
    spi_len--;
    if (spi_len > 0) SPI1->DR = *spi_buf;
}

void TIM2_IRQ(void) {
    TIM2->SR &= ~TIM_SR_UIF;
    ADC1->CR2 |= ADC_CR2_SWSTART;
}

void HARDFAULT_IRQ(void) {
    void *ptr;

    __asm(
            "TST lr, #4 \n"
            "ITE EQ \n"
            "MRSEQ %[ptr], MSP  \n"
            "MRSNE %[ptr], PSP  \n"
            : [ptr] "=r" (ptr)
            );

    memcpy(&fault_trace, ptr, sizeof (fault_trace));
    scb_hfsr = SCB->HFSR;
    scb_cfsr = SCB->CFSR;
    scb_bfar = SCB->BFAR;
    fault_trace_flag = FAULT_TRACE_FLAG;

    chip_reset();
}

