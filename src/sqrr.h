/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   sqrr.h
 * Author: alex
 *
 * Created on 1 февраля 2022 г., 7:58
 */

#ifndef SQRR_H
#define SQRR_H

#include <stdint.h>

uint32_t sqrr(uint32_t a_nInput);

#endif /* SQRR_H */

