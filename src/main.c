
#include "chip.h"
#include "dsp/stm32_dsp.h"
#include "dsp/table_fft.h"
#include "sqrr.h"
#include <math.h>

#define Flow        300
#define Fmidl       1000
#define Fmidh       3000
#define Fhigh       5000

#define NOISE_LEVEL 70

#define MEAN_COEFF  1000
#define NPT         256     // 6400uS
#define Fs          40000

typedef union {

    struct {
        unsigned buf : 1;
        unsigned upd1 : 1;
        unsigned upd2 : 1;
        unsigned b_det : 1;
    };
    uint8_t v;
}
FLAGS;

static FLAGS flags;

static uint8_t sine_table[256];
static int r = 0, g = 0, b = 0;

static uint16_t ADCArray1[NPT];
static uint16_t ADCArray2[NPT];

static float mean = 1700;

static uint32_t lBufInArray[NPT];
static uint32_t lBufOutArray[NPT];

static int maxR = 0, maxG = 0, maxB = 0;

void pwm_init(void);
void sample_init(void);
void sample_start(void);

void GetPowerMag();

int main() {

    chip_init();
    flags.v = 0;

    dbg_write("\fstm32-lights\r\n");

    for (int i = 0; i < sizeof (sine_table); i++)
        sine_table[i] = sin(i * (2 * M_PI / sizeof (sine_table)) + (3 * M_PI / 2)) * 125 + 130;

    GPIOC->BSRR = BIT13; // LED off

    pwm_init();

    sample_init();
    sample_start();

    for (;;) {

        int16_t *ADCArray = 0;
        if (flags.upd1) {
            ADCArray = (int16_t*) ADCArray1;
            flags.upd1 = 0;
        }
        if (flags.upd2) {
            ADCArray = (int16_t*) ADCArray2;
            flags.upd2 = 0;
        }
        if (ADCArray > 0) {
            GPIOC->BSRR = BIT13; // LED off
            mean -= mean / MEAN_COEFF;
            mean += ((float) ADCArray[0]) / MEAN_COEFF;
            for (int i = 0; i < NPT; i++)
                lBufInArray[i] = (ADCArray[i] - (int) mean) << 17;
            cr4_fft_256_stm32(lBufOutArray, lBufInArray, NPT);
            GetPowerMag();
        }

        switch (get_timer(TMR_CMU)) {
            case TMR_RUN:
                if (ADCArray == 0) break;
                TIM2->CCR4 = maxR;
                TIM2->CCR3 = maxG;
                TIM2->CCR2 = maxB;
                break;
            case TMR_ONCE:
                r = 0, g = 0, b = 0;
                break;
            case TMR_IDLE:
                if (get_timer(TMR_LEDR) != TMR_RUN) {
                    set_timer(TMR_LEDR, 101);
                    TIM2->CCR4 = sine_table[r];
                    r++;
                    if (r >= sizeof (sine_table)) r = 0;
                }
                if (get_timer(TMR_LEDG) != TMR_RUN) {
                    set_timer(TMR_LEDG, 103);
                    TIM2->CCR3 = sine_table[g];
                    g++;
                    if (g >= sizeof (sine_table)) g = 0;
                }
                if (get_timer(TMR_LEDB) != TMR_RUN) {
                    set_timer(TMR_LEDB, 107);
                    TIM2->CCR2 = sine_table[b];
                    b++;
                    if (b >= sizeof (sine_table)) b = 0;
                }
                break;
        }

    }
}

void GetPowerMag() {

    maxR = 0;
    maxG = 0;
    maxB = 0;

    float corr = 1;

    for (int i = 1; i < NPT / 2; i++) {

        int16_t lX = lBufOutArray[i] & 0xffff;
        int16_t lY = lBufOutArray[i] >> 16;

        float Mag = sqrr(lX * lX + lY * lY);
        Mag *= corr;

        if (Mag > NOISE_LEVEL) Mag -= NOISE_LEVEL;
        else Mag = 0;

        int f = Fs * i / NPT;
        if (f < Flow) {
            if (Mag > maxR) maxR = Mag;
        }
        if ((f > Fmidl) && (f < Fmidh)) {
            corr = 1.1;
            if (Mag > maxG) maxG = Mag;
        }
        if (f > Fhigh) {
            corr = 4;
            if (Mag > maxB) maxB = Mag;
        }
    }

    if (maxR && maxG && maxB) set_timer(TMR_CMU, 30000);

    if (maxR > PWM_MAX) maxR = PWM_MAX;
    if (maxG > PWM_MAX) maxG = PWM_MAX;
    if (maxB > PWM_MAX) maxB = PWM_MAX;
}

void usart_rx_byte(uint8_t b) {

}

void pwm_init(void) {

    /* LED PWR PB7 */

    GPIOB->CRL &= 0x0fffffff;
    GPIOB->CRL |= 0x20000000;
    GPIOB->BSRR = BIT7;

    /* TIM2 PWM */

    GPIOA->CRL &= 0xffff0000; // A0:Reserverd, A1:B, A2:G, A3:R
    GPIOA->CRL |= 0x0000bbbb;

    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;

    TIM2->CCMR1 = TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1;
    TIM2->CCMR2 = TIM_CCMR2_OC4M_2 | TIM_CCMR2_OC4M_1 | TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1;
    TIM2->CCER = TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC4E
            | TIM_CCER_CC1P | TIM_CCER_CC2P | TIM_CCER_CC3P | TIM_CCER_CC4P;
    TIM2->PSC = 4;
    TIM2->ARR = PWM_MAX;
    TIM2->CR1 = TIM_CR1_CEN; // CCR1:Reserverd, CCR2:B, CCR3:G, CCR4:R

    TIM2->CCR1 = PWM_MAX / 2;
}

void sample_init(void) {

    /* TIM3 */

    RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;

    TIM3->ARR = 72000000 / Fs;
    TIM3->CR2 = TIM_CR2_MMS_1;
    TIM3->CR1 = TIM_CR1_CEN;

    /* ADC */

    GPIOB->CRL &= 0xffffff0f; // B1 - A9

    RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;

    ADC1->CR2 |= ADC_CR2_ADON;
    for (uint32_t i = 0; i < 1000; i++) __asm__ volatile ("nop");
    ADC1->CR2 |= ADC_CR2_CAL;
    while (ADC1->CR2 & ADC_CR2_CAL);
    ADC1->SQR3 = ADC_SQR3_SQ1_3 | ADC_SQR3_SQ1_0;
    ADC1->CR2 |= ADC_CR2_EXTTRIG | ADC_CR2_EXTSEL_2 | ADC_CR2_DMA; // TIM3 DMA

    /* DMA1 */

    RCC->AHBENR |= RCC_AHBENR_DMA1EN;

    DMA1_Channel1->CCR = DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE_0 | DMA_CCR_MINC | DMA_CCR_TCIE;
    DMA1_Channel1->CPAR = (uint32_t) & ADC1->DR;

    __NVIC_EnableIRQ(DMA1_Channel1_IRQn);
}

void sample_start(void) {
    DMA1_Channel1->CNDTR = NPT;
    if (flags.buf) DMA1_Channel1->CMAR = (uint32_t) ADCArray2;
    else DMA1_Channel1->CMAR = (uint32_t) ADCArray1;
    DMA1_Channel1->CCR |= DMA_CCR_EN;
}

void DMA1_Channel1_IRQ(void) {
    DMA1->IFCR = DMA_IFCR_CTCIF1;
    DMA1_Channel1->CCR &= ~DMA_CCR_EN;
    if (flags.buf) flags.upd2 = 1;
    else flags.upd1 = 1;
    flags.buf ^= 1;
    GPIOC->BRR = BIT13; // LED on
    sample_start();
}
